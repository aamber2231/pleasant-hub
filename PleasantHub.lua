-- Load the UI library	
loadstring(game:HttpGet('https://pastebin.com/raw/cwDSpepQ', true))()

local Fonts = {};
for Font, _ in next, Drawing.Fonts do
	table.insert(Fonts, Font);
end;

-- Declare the game/player variables.
local workspace = game:GetService('Workspace')
local players = game:GetService('Players')
local RunService = game:GetService("RunService")


-- Declare the UI library variables.
local window = library:AddWindow('Pleasant Hub', {
	main_color = Color3.fromRGB(41, 74, 122),
	min_size = Vector2.new(120, 120),
	toggle_key = Enum.KeyCode.RightShift,
	can_resize = true,
})
local tab_player = window:AddTab('Player')
local tab_misc = window:AddTab('Misc')
local tab_player_moveSpeed_slider = tab_player:AddSlider('Movement Speed', function ( value )
	local humanoid = players.LocalPlayer.Character:FindFirstChild('Humanoid')
	if humanoid then
		humanoid.WalkSpeed = value
	end
end, { min = 16, max = 450, readonly = false })
-- local tab_player_unban_textBox = tab_player:AddTextBox('Banned house name', { clear = false })
-- local tab_player_unban_button = tab_player:AddButton('Unban from house', function (  )
-- 	local houses = workspace:FindFirstChild('001_Lots')
-- 	if houses then
-- 		for _ , house in ipairs(houses:GetChildren()) do
-- 			if string.find(house.Name:lower(), tab_player_unban_textBox.text:lower()) then
-- 				local picked = house:FindFirstChild('HousePickedByPlayer')
-- 				local model = picked:FindFirstChild('HouseModel')
-- 				for _,object in ipairs(model:GetChildren()) do
-- 					if string.find(object.Name, 'BannedBlock') then
-- 						object:Remove()
-- 					end
-- 				end
-- 			end
-- 		end
-- 	end
-- end)
local tab_player_unban_all_houses_button = tab_player:AddButton('Unban from all houses', function (  )
	local houses = workspace:FindFirstChild('001_Lots')
	if houses then
		for _ , house in ipairs(houses:GetChildren()) do
			local picked = house:FindFirstChild('HousePickedByPlayer')
			if picked then
				local model = picked:FindFirstChild('HouseModel')
				for _,object in ipairs(model:GetChildren()) do
					if string.find(object.Name, 'BannedBlock') then
						object:Remove()
					end
				end
			end
		end
	end
end)
local tab_player_house_teleport_textBox = tab_player:AddTextBox('House name', { clear = false })
local tab_player_house_teleport_button = tab_player:AddButton('Teleport to house', function (  )
	local houses = workspace:FindFirstChild('001_Lots')
	if houses then
		for _ , house in ipairs(houses:GetChildren()) do
			if string.find(house.Name:lower(), tab_player_house_teleport_textBox.text:lower()) then
				local spawnPosition = house:FindFirstChild('HouseSpawnPosition')
				players.LocalPlayer.Character.HumanoidRootPart.CFrame = spawnPosition.CFrame
			end
		end
	end
end)
local tab_player_player_teleport_textBox = tab_player:AddTextBox('Player name', { clear = false })
local tab_player_player_teleport_button = tab_player:AddButton('Teleport to player', function (  )
	for _,player in ipairs(players:GetChildren()) do
		if string.find(player.Name:lower(), tab_player_player_teleport_textBox.text:lower()) then
			players.LocalPlayer.Character.HumanoidRootPart.CFrame = player.Character.HumanoidRootPart.CFrame
		end
	end
end)
local alwaysTeleport = false
local tab_player_autoclicker_teleport_switch = tab_player:AddSwitch("Always Teleport", function ( bool )
	alwaysTeleport = bool
end)
local tab_player_funnypacket_button = tab_player:AddButton('Send Shoulders Request', function (  )
	if players then
		for _,player in ipairs(players:GetChildren()) do
			if player.Name ~= players.LocalPlayer.Name then
				-- print(player,player.Name)
				local A_1 = "Client2Client"
				local A_2 = "Request: Shoulders!"
				local A_3 = game:GetService("Players"):FindFirstChild(player.Name)
				local Event = game:GetService("ReplicatedStorage").RemoteEvents.P17layerTriggerE17vent
				Event:FireServer(A_1, A_2, A_3)
			end
		end
	end
end)
-- local tab_player_housenumber_textbox = tab_player:AddTextBox('House Number', { clear = false }
local housenumber = 0
local tab_player_roomate_dropdown = tab_player:AddDropdown("House Number", function ( text )
	housenumber = tonumber(text)
end)
local house1 = tab_player_roomate_dropdown:Add("1")
local house2 = tab_player_roomate_dropdown:Add("2")
local house3 = tab_player_roomate_dropdown:Add("3")
local house4 = tab_player_roomate_dropdown:Add("4")
local house5 = tab_player_roomate_dropdown:Add("5")
local house6 = tab_player_roomate_dropdown:Add("6")
local house7 = tab_player_roomate_dropdown:Add("7")
local house8 = tab_player_roomate_dropdown:Add("8")
local house9 = tab_player_roomate_dropdown:Add("9")
local house10 = tab_player_roomate_dropdown:Add("10")
local house11 = tab_player_roomate_dropdown:Add("11")
local house12 = tab_player_roomate_dropdown:Add("12")
local house13 = tab_player_roomate_dropdown:Add("13")
local house14 = tab_player_roomate_dropdown:Add("14")
local house15 = tab_player_roomate_dropdown:Add("15")
local house16 = tab_player_roomate_dropdown:Add("16")
local house17 = tab_player_roomate_dropdown:Add("17")
local house18 = tab_player_roomate_dropdown:Add("18")
local house19 = tab_player_roomate_dropdown:Add("19")
local house20 = tab_player_roomate_dropdown:Add("20")
local house21 = tab_player_roomate_dropdown:Add("21")
local house22 = tab_player_roomate_dropdown:Add("22")
local house23 = tab_player_roomate_dropdown:Add("23")
local house24 = tab_player_roomate_dropdown:Add("24")
local house25 = tab_player_roomate_dropdown:Add("25")
local house26 = tab_player_roomate_dropdown:Add("26")
local house27 = tab_player_roomate_dropdown:Add("27")
local house28 = tab_player_roomate_dropdown:Add("28")
local house29 = tab_player_roomate_dropdown:Add("29")
local house30 = tab_player_roomate_dropdown:Add("30")
local house31 = tab_player_roomate_dropdown:Add("31")
local tab_player_giveroomate_button = tab_player:AddButton('Give Roomate', function (  )
	local name = players.LocalPlayer.Name
	
	local A_1 = "GivePermissionLoopToServer"
	local A_2 = game:GetService("Players"):FindFirstChild(name)
	local A_3 = housenumber
	local Event = game:GetService("ReplicatedStorage").RemoteEvents.P17layerTriggerE17vent
	Event:FireServer(A_1, A_2, A_3)
end)

RunService.RenderStepped:Connect(function(step)
	if (alwaysTeleport) then
		for _,player in ipairs(players:GetChildren()) do
			if string.find(player.Name:lower(), tab_player_player_teleport_textBox.text:lower()) then
			players.LocalPlayer.Character.HumanoidRootPart.CFrame = player.Character.HumanoidRootPart.CFrame
			end
		end
	end
end)
local tab_misc_executeinfyield_button = tab_misc:AddButton('Load Inf Yield', function (  )
	loadstring(game:HttpGet('https://gitlab.com/aamber2231/pleasant-hub/-/raw/main/infyield.lua', true))()
end)
